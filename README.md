# SP_Class_Boundaries


To train a convolutional neural network in Pytorch (or Keras) on the CIFAR-10 dataset (find a tutorial online if necessary). 
You are going to train 2 versions of that network (using the same optimizer, learning rate, number
of epochs, etc) where the only thing that is going to change is the normalization done to
the data, before being passed to the network: 
1. The first version has to normalize the data
such that the entire dataset has a mean of zero and a standard deviation of one (those
statistics are computed over the training set, where each image is considered one
sample). 
2. The second version will use samples that lie in the unit cube (that is, values
range between 0 –for black, and 1 –for white). Report the curves for “accuracy” and “loss”
during training (same for a validation set).



**Libraries Used:**
1. Tensorflow
2. Keras
3. Matplotlib
4. sklearn
5. skimage


Training CNN in Tensorflow on CIFAR-10 dataset.It consists of 60000 32x32 colour images in 10 classes, with 6000 images per class.
There are 50000 training images and 10000 test images. We imported the dataset from tensorflow datasets for this task.

**In this task, we performed 3 kinds of preprocessing**


*  Section 1: Standardization - mean 0, Standard Deviation 1
	mean and standard deviation for the whole training samples are calculated to standardize the training dataset to have mean of 0 and standard deviation of 1.

*  Section 2: Normalization - dataset should lie in RGB unit cube
	all the RGB pixel values of each training sample is normalized to have value from 0 to 1

*  Section 3: Normalization - dataset should lie in unit cube (0 for black, 1 for white)
	The images are converted to gray scale and normalized to have the values in the range of 0 to 1

![](Outputs/grayscale.PNG)

the color images are converted to grayscale : sample output
# 3 models are developed with same training parameters
Training Hyperparameter :
1. Epochs : 30 , 50
2. Optimizer : Adam
3. Metric : categorical_crossentrophy

**Note:  Hyperparameters tuning is not done**


**Accuracy and loss plots are plotted for checking model performance.**
*  Section 1:Standardisation with mean-0 and std deviation-1 

![](Outputs/firstversion.PNG)

*  Section 2:Normalization RGB unit cube- between 0 and 1

![](Outputs/seconfversioncolor.PNG)

*  Section 3:Normalization grayscaled unit cube 

![](Outputs/graylossandaccuracy.PNG)